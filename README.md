# MSG: Meaningful Swing GUI
This is a library that aims to allow creation of a more dynamic and better looking GUI using the Swing toolkit.
The secondary aims are to introduce some modern GUI building concepts and to offer possibilities for production of cleaner code.

![](projectLogo.png)

## Functions and Benefits
- One of the main functions of MSG is the `Component reorganization`. Instead of fixed sizing and positioning,
the size and position of a `Component` is determined by a set of coefficients (the `Component Frame`) along with
hierarchy ties between the `Components`.
This way an MSG **application can run on a display of any resolution and handle
runtime changes of display resolution as well**.
- To tune up the look of the application, the library `Components` offer simple texturing methods.
Unlike standard Swing, most `Components` also support alpha-channel textures.
This combination makes is possible to **come up with sleek designs for GUIs built upon the Swing toolkit**.
- Manipulate flawlessly the state of `Components` within the hierarchy using customizable methods like
`Update Component` and more ...

## Installation
Check the Jitpack.io (use the link below) for the newest artifact. Jitpack offers support for all major build tools.

[![](https://jitpack.io/v/com.gitlab.MichalBures_OG/MSG.svg)](https://jitpack.io/#com.gitlab.MichalBures_OG/MSG)

## Usage and Code examples
### 1. Simple undecorated 'Hello World' window
For this code to work as intended, please use:
- Kotlin 1.7.10
- MSG 1.4.5

``` kotlin
import MSG.Components.MButton
import MSG.Components.MFrame
import MSG.Components.MLabel

import MSG.DataTransformation.ComponentFrame

import MSG.Miscellaneous.ResolutionControl

fun main() {

    val simpleWindow = MFrame(ComponentFrame(0.3, 0.3, 0.4, 0.4), true, false, "Simple Window")
    simpleWindow.add(MLabel(ComponentFrame(0.1, 0.1, 0.8, 0.3)).apply {
        text = "Hello World"
        setFontSizeModifier(5f)
    })
    simpleWindow.add(MButton(ComponentFrame(0.2, 0.5, 0.6, 0.4), true, true, null, null).apply {
        addOnClickReaction { simpleWindow.terminateFrame() }
        labelText = "Close this window"
        setLabelFontSizeModifier(3f)
    })
    simpleWindow.isVisible = true

    ResolutionControl.setupResolutionControl({ newParentSize -> simpleWindow.reorganize(newParentSize) }, 1)
}
```

## Credit
MSG is being worked on solely by Michal Bureš
 - Software Developer at [IXPERTA s.r.o.](https://ixperta.com/)
 - Computer science student at [BUT FIT](https://www.fit.vut.cz/.en)

## License
MSG is being distributed under the [MIT](https://choosealicense.com/licenses/mit/) licence.

package MSG.DataProcessing;

/**
 * @author Michal Bureš 2020-2022
 * Contains some useful transformation and calculation methods regarding double data type (Do not implement within classes!)
 */
public interface DoubleTransformer {

    /* ■■■■■■■■■■■■■■■■■■■■ Public API - static interface methods ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param dividend The number that will be divided
     * @param divisor  The number to divide the first number with
     * @return The fraction obtained by dividing the dividend by divisor as a double data type
     * Use to quickly and easily obtain highly accurate double values from integer fractions
     */
    static double fractionToDouble(int dividend, int divisor) {
        final double COEFFICIENT = 10e15d;
        return (double) Math.round(((double) dividend / (double) divisor) * COEFFICIENT) / COEFFICIENT;
    }
}

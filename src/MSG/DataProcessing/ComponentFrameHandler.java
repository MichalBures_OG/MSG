package MSG.DataProcessing;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Imports from within the project
import MSG.DataTransformation.ComponentFrame;

// Import Java AWT
import java.awt.*;

/**
 * @author Michal Bureš 2020-2022
 * Utility used to manipulate the ComponentFrame data transformation object in various ways
 */
public interface ComponentFrameHandler {

    /* ■■■■■■■■■■■■■■■■■■■■ Public API - static methods ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param componentFrame The ComponentFrame structure of the child component
     * @param parentComponentSize The size of the parent component
     * Generate Swing boundaries from ComponentFrame structure data and parent component size information
     */
    static Rectangle generateBoundaries(ComponentFrame componentFrame, Dimension parentComponentSize) {
        return new Rectangle(
                (int) Math.round(componentFrame.x * parentComponentSize.width),
                (int) Math.round(componentFrame.y * parentComponentSize.height),
                (int) Math.round(componentFrame.w * parentComponentSize.width),
                (int) Math.round(componentFrame.h * parentComponentSize.height)
        );
    }
}

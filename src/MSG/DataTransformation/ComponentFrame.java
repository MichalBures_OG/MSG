package MSG.DataTransformation;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Project Lombok imports
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

// In-project imports
import MSG.DataProcessing.DoubleTransformer;

/**
 * @author Michal Bureš 2020-2022
 * Storage object storing the data regarding size and positioning of a component
 */
@AllArgsConstructor
public class ComponentFrame {

    /* ■■■■■■■■■■■■■■■■■■■■ Constant fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Component frame of the parent component
     */
    @Deprecated
    public static final ComponentFrame PARENT_FRAME = new ComponentFrame(0d, 0d, 1d, 1d);

    /**
     * Center point for component position
     */
    public static final int CENTER = 0;

    /**
     * Upper left point for component position
     */
    public static final int STANDARD = 1;

    /**
     * Lower right point for component position
     */
    public static final int INVERSE = 2;

    /* ■■■■■■■■■■■■■■■■■■■■ Variable fields - the instance specific data ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * X-axis shift of the component origin relative to parent component
     */
    @Setter
    @Getter
    public double x;

    /**
     * Y-axis shift of the component origin relative to parent component
     */
    @Setter
    @Getter
    public double y;

    /**
     * X-axis size of the component relative to parent component
     */
    @Setter
    @Getter
    public double w;

    /**
     * Y-axis size of the component relative to parent component
     */
    @Setter
    @Getter
    public double h;

    /* ■■■■■■■■■■■■■■■■■■■■ Static constructors ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Component frame of the parent component
     */
    public static ComponentFrame ofParentComponent() {

        return new ComponentFrame(0d, 0d, 1d, 1d);
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Custom constructors ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param sizeCoefficient Parent size coefficient - shared between width and height
     * @param positionOrigin Point necessary for component position determination
     * Constructor that allows for a simple creation of most common component frames
     */
    public ComponentFrame(double sizeCoefficient, int positionOrigin) {
        switch (positionOrigin) {
            case CENTER:
                x = (1d - sizeCoefficient) / 2;
                y = (1d - sizeCoefficient) / 2;
                break;
            case STANDARD:
                x = 0d;
                y = 0d;
                break;
            case INVERSE:
                x = 1d - sizeCoefficient;
                y = 1d - sizeCoefficient;
        }
        w = sizeCoefficient;
        h = sizeCoefficient;
    }

    /**
     * @param w Parent size coefficient - used by width
     * @param h Parent size coefficient - used by height
     * @param positionOrigin Point necessary for component position determination
     * Constructor that allows for a more elegant definition of all component frames without offset
     */
    public ComponentFrame(double w, double h, int positionOrigin) {
        switch (positionOrigin) {
            case CENTER:
                x = (1d - w) / 2;
                y = (1d - h) / 2;
                break;
            case STANDARD:
                x = 0d;
                y = 0d;
                break;
            case INVERSE:
                x = 1d - w;
                y = 1d - h;
        }
        this.w = w;
        this.h = h;
    }

    // TODO: Document these new constructors as well?

    public ComponentFrame(int[] x, int[] y, int[] w, int[] h) {

        this.x = x.length == 2 ? DoubleTransformer.fractionToDouble(x[0], x[1]) : 0d;
        this.y = y.length == 2 ? DoubleTransformer.fractionToDouble(y[0], y[1]) : 0d;
        this.w = w.length == 2 ? DoubleTransformer.fractionToDouble(w[0], w[1]) : 0d;
        this.h = h.length == 2 ? DoubleTransformer.fractionToDouble(h[0], h[1]) : 0d;
    }

    public ComponentFrame(int[] x, double y, double w, double h) {

        this.x = x.length == 2 ? DoubleTransformer.fractionToDouble(x[0], x[1]) : 0d;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public ComponentFrame(double x, int[] y, double w, double h) {

        this.x = x;
        this.y = y.length == 2 ? DoubleTransformer.fractionToDouble(y[0], y[1]) : 0d;
        this.w = w;
        this.h = h;
    }

    public ComponentFrame(double x, double y, int[] w, double h) {

        this.x = x;
        this.y = y;
        this.w = w.length == 2 ? DoubleTransformer.fractionToDouble(w[0], w[1]) : 0d;
        this.h = h;
    }

    public ComponentFrame(double x, double y, double w, int[] h) {

        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h.length == 2 ? DoubleTransformer.fractionToDouble(h[0], h[1]) : 0d;
    }

    public ComponentFrame(int[] x, int[] y, double w, double h) {

        this.x = x.length == 2 ? DoubleTransformer.fractionToDouble(x[0], x[1]) : 0d;
        this.y = y.length == 2 ? DoubleTransformer.fractionToDouble(y[0], y[1]) : 0d;
        this.w = w;
        this.h = h;
    }

    public ComponentFrame(double x, int[] y, int[] w, double h) {

        this.x = x;
        this.y = y.length == 2 ? DoubleTransformer.fractionToDouble(y[0], y[1]) : 0d;
        this.w = w.length == 2 ? DoubleTransformer.fractionToDouble(w[0], w[1]) : 0d;
        this.h = h;
    }

    public ComponentFrame(double x, double y, int[] w, int[] h) {

        this.x = x;
        this.y = y;
        this.w = w.length == 2 ? DoubleTransformer.fractionToDouble(w[0], w[1]) : 0d;
        this.h = h.length == 2 ? DoubleTransformer.fractionToDouble(h[0], h[1]) : 0d;
    }

    public ComponentFrame(int[] x, double y, double w, int[] h) {

        this.x = x.length == 2 ? DoubleTransformer.fractionToDouble(x[0], x[1]) : 0d;
        this.y = y;
        this.w = w;
        this.h = h.length == 2 ? DoubleTransformer.fractionToDouble(h[0], h[1]) : 0d;
    }

    public ComponentFrame(double x, int[] y, double w, int[] h) {

        this.x = x;
        this.y = y.length == 2 ? DoubleTransformer.fractionToDouble(y[0], y[1]) : 0d;
        this.w = w;
        this.h = h.length == 2 ? DoubleTransformer.fractionToDouble(h[0], h[1]) : 0d;
    }

    public ComponentFrame(int[] x, double y, int[] w, double h) {

        this.x = x.length == 2 ? DoubleTransformer.fractionToDouble(x[0], x[1]) : 0d;
        this.y = y;
        this.w = w.length == 2 ? DoubleTransformer.fractionToDouble(w[0], w[1]) : 0d;
        this.h = h;
    }

    public ComponentFrame(double x, int[] y, int[] w, int[] h) {

        this.x = x;
        this.y = y.length == 2 ? DoubleTransformer.fractionToDouble(y[0], y[1]) : 0d;
        this.w = w.length == 2 ? DoubleTransformer.fractionToDouble(w[0], w[1]) : 0d;
        this.h = h.length == 2 ? DoubleTransformer.fractionToDouble(h[0], h[1]) : 0d;
    }

    public ComponentFrame(int[] x, int[] y, int[] w, double h) {

        this.x = x.length == 2 ? DoubleTransformer.fractionToDouble(x[0], x[1]) : 0d;
        this.y = y.length == 2 ? DoubleTransformer.fractionToDouble(y[0], y[1]) : 0d;
        this.w = w.length == 2 ? DoubleTransformer.fractionToDouble(w[0], w[1]) : 0d;
        this.h = h;
    }

    public ComponentFrame(int[] x, int[] y, double w, int[] h) {

        this.x = x.length == 2 ? DoubleTransformer.fractionToDouble(x[0], x[1]) : 0d;
        this.y = y.length == 2 ? DoubleTransformer.fractionToDouble(y[0], y[1]) : 0d;
        this.w = w;
        this.h = h.length == 2 ? DoubleTransformer.fractionToDouble(h[0], h[1]) : 0d;
    }

    public ComponentFrame(int[] x, double y, int[] w, int[] h) {

        this.x = x.length == 2 ? DoubleTransformer.fractionToDouble(x[0], x[1]) : 0d;
        this.y = y;
        this.w = w.length == 2 ? DoubleTransformer.fractionToDouble(w[0], w[1]) : 0d;
        this.h = h.length == 2 ? DoubleTransformer.fractionToDouble(h[0], h[1]) : 0d;
    }
}

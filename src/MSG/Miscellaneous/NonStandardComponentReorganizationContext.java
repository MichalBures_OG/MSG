package MSG.Miscellaneous;

import MSG.DataProcessing.ComponentFrameHandler;
import MSG.DataTransformation.ComponentFrame;

import java.awt.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public class NonStandardComponentReorganizationContext {

    private static final Map<Component, ComponentFrame> componentFrameRegister = new HashMap<>();

    private static final Predicate<Component> incapableOfReorganization = component -> !(component instanceof ReorganizationCapable);

    public static void registerComponentFrame(Object nonStandardComponent, ComponentFrame componentFrame) {

        if (nonStandardComponent instanceof Component) {
            componentFrameRegister.put(((Component) nonStandardComponent), componentFrame);
        }
    }

    public static void reorganizeNonStandardComponents(Object current) {

        if (current instanceof Container) {
            Container currentContainer = ((Container) current);
            Arrays.stream(currentContainer.getComponents())
                    .filter(incapableOfReorganization)
                    .filter(componentFrameRegister::containsKey)
                    .peek(component -> {
                        ComponentFrame componentFrame = componentFrameRegister.get(component);
                        Dimension currentContainerSize = currentContainer.getSize();
                        component.setBounds(ComponentFrameHandler.generateBoundaries(componentFrame, currentContainerSize));
                    })
                    .forEach(NonStandardComponentReorganizationContext::reorganizeNonStandardComponents);
        }
    }
}

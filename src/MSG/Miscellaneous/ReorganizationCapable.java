package MSG.Miscellaneous;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Import Java AWT
import java.awt.*;

/**
 * @author Michal Bureš 2020-2022
 * This interface maintains the most significant capability of the MSG components: reorganization (All MSG components implement this interface)
 */
public interface ReorganizationCapable {

    /* ■■■■■■■■■■■■■■■■■■■■ Interface methods ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param newParentSize The size of parent component
     * Adjust the position and size of a component and send the call downwards
     */
    default void reorganize(Dimension newParentSize) {
        reorganizeComponent(newParentSize);
        reorganizeChildren();
        NonStandardComponentReorganizationContext.reorganizeNonStandardComponents(this);
    }

    /**
     * @param newParentSize The size of the parent component
     * Adjust the position and size of a component accordingly to the parent component
     */
    void reorganizeComponent(Dimension newParentSize);

    /**
     * Pass the reorganization call one layer downwards
     */
    default void reorganizeChildren() {
    }

    /**
     * An empty, overridable method that might come in handy when defining custom components using anonymous classes
     */
    default void updateComponent() {
    }
}

package MSG.Miscellaneous;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// IO related imports
import javax.imageio.ImageIO;
import java.io.PrintStream;
import java.io.IOException;

// Standard Buffered Image import
import java.awt.image.BufferedImage;

// Object utilities import
import java.util.Objects;

/**
 * @author Michal Bureš 2020-2022
 * This interface offers some utilities regarding filesystem resources utilized by application graphics
 */
public interface GraphicsResourceUtilities {

    /**
     * Safely load an image resource. The simple and quiet mode.
     *
     * @param resourceIdentifier Image resource identifier string
     * @return BufferedImage instance or null if not found
     */
    default BufferedImage loadImageResource(final String resourceIdentifier) {

        BufferedImage imageResource = null;
        try {
            imageResource = ImageIO.read(Objects.requireNonNull(getClass().getResource(resourceIdentifier)));
        } catch (NullPointerException | IOException ignored) {
        }
        return imageResource;
    }

    /**
     * Safely load an image resource. The standard mode.
     *
     * @param resourceIdentifier Image resource identifier string
     * @param printStream The system print-stream to which any error messages will be printed
     * @param printStackTrace Should the method print full stack-trace or just the exception message?
     * @return BufferedImage instance or null if not found
     */
    default BufferedImage loadImageResource(final String resourceIdentifier, PrintStream printStream, boolean printStackTrace) {

        BufferedImage imageResource = null;
        try {
            imageResource = ImageIO.read(Objects.requireNonNull(getClass().getResource(resourceIdentifier)));
        } catch (NullPointerException | IOException exception) {
            printStream.println(exception.getMessage());
            if (printStackTrace) exception.printStackTrace(printStream);
        }
        return imageResource;
    }
}

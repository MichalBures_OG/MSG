package MSG.Miscellaneous;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Project Lombok imports
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

// Import Java AWT
import java.awt.*;

// Import concurrent programming utilities
import java.util.concurrent.*;
import java.util.function.Consumer;

/**
 * @author Michal Bureš 2020-2022
 * Environment status manager - checks for monitor resolution changes and initiates application reorganization call on detection
 */
@NoArgsConstructor
public class ResolutionControl {

    /* ■■■■■■■■■■■■■■■■■■■■ Constant fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Concurrent programming utility - allows scheduling of tasks
     */
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    /**
     * Java toolkit package - contains some very useful utility methods (here to obtain the resolution of the primary system monitor)
     */
    private final Toolkit defaultToolkit = Toolkit.getDefaultToolkit();

    /* ■■■■■■■■■■■■■■■■■■■■ Variable fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * The user specified monitor to consider an environment of the application
     */
    @Getter
    @Setter
    private GraphicsDevice specificMonitor = null;

    /* ■■■■■■■■■■■■■■■■■■■■ Inner variable fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * The monitor resolution property - undergoing a periodical check
     */
    @Getter
    private Dimension screenResolution = new Dimension(-1, -1);

    /* ■■■■■■■■■■■■■■■■■■■■ Private utility methods ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Updates the screenResolution property before checking for the property value change
     */
    private void updateScreenResolution() {
        if (specificMonitor == null) {
            screenResolution = defaultToolkit.getScreenSize();
        } else {
            Rectangle screenBoundaries = specificMonitor.getDefaultConfiguration().getBounds();
            screenResolution = new Dimension(screenBoundaries.width, screenBoundaries.height);
        }
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Public API ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Handy static method to simplify the Resolution Control setup
     *
     * @param appReorganization Set up the reorganization call for your app with this Consumer interface
     * @param checkPeriodInSeconds The period between resolution checks in seconds
     */
    public static void setupResolutionControl(Consumer<Dimension> appReorganization, int checkPeriodInSeconds) {

         new ResolutionControl() {

             @Override
             public void reorganizeApplication(Dimension newParentSize) {
                 appReorganization.accept(newParentSize);
             }
         }.initiateResolutionCheck(checkPeriodInSeconds);
    }
    
    /**
     * @param checkPeriodInSeconds The period between the resolution checks in seconds
     * Routine method - checks the monitor resolution (application environment) in a separate thread (necessary to call on application startup)
     */
    public void initiateResolutionCheck(int checkPeriodInSeconds) {
        scheduler.scheduleAtFixedRate(new Thread(() -> {
            Dimension previousScreenResolution = screenResolution;
            updateScreenResolution();
            if (!screenResolution.equals(previousScreenResolution)) {
                reorganizeApplication(screenResolution);
            }
        }), 0, checkPeriodInSeconds, TimeUnit.SECONDS);
    }

    /**
     * @param newParentSize The changed resolution of the specific monitor (environment)
     * Origin of the application reorganization call - override as you need (Should target all used MFrame instances)
     */
    public void reorganizeApplication(Dimension newParentSize) {
    }
}

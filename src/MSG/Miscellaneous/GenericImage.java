package MSG.Miscellaneous;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Import Java AWT
import java.awt.*;

// Import Buffered Image
import java.awt.image.BufferedImage;

/**
 * @author Michal Bureš 2020-2022
 * A simplified version of BufferedImage: a single color image with transparency feature
 */
public class GenericImage extends BufferedImage {

    /* ■■■■■■■■■■■■■■■■■■■■ Constant fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Default filling color of MSG components
     */
    public static final Color DEFAULT_FILL = new Color(223, 223, 223, 255);

    /**
     * Secondary filling color of MSG components
     */
    public static final Color SECONDARY_FILL = new Color(134, 134, 134, 255);

    /**
     * Default highlighting color of MSG components
     */
    public static final Color DEFAULT_HIGHLIGHT = new Color(76, 246, 140, 255);

    /* ■■■■■■■■■■■■■■■■■■■■ Public API - constructor ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param color The color the newly created Image should be filled with
     * Construct a new instance simply by passing in the desired color
     */
    public GenericImage(Color color) {
        super(100, 100, BufferedImage.TYPE_4BYTE_ABGR);
        setColor(color);
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Additional private utility methods ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * This method sets the specified color of the Image within the graphics context
     * @param color Color to fill the image with
     */
    private void setColor(Color color) {
        Graphics2D graphicsContext = createGraphics();
        graphicsContext.setPaint(color);
        graphicsContext.fillRect(0, 0, getWidth(), getHeight());
    }
}

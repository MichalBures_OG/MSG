package MSG.Components;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

import MSG.DataProcessing.ComponentFrameHandler;
import MSG.DataTransformation.ComponentFrame;
import MSG.Miscellaneous.ReorganizationCapable;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

/**
 * @author Michal Bureš 2020-2022
 * Enhanced variant of Swing JList - comes with component re-scaling and font re-scaling
 */
public class MList<E> extends JList<E> implements ReorganizationCapable {

    /**
     * How big should the font be? (This property must be set - otherwise the font will not be visible!)
     */
    @Setter
    private float fontSizeModifier = 0f;

    /**
     * Component frame instance used by this MList
     */
    @Getter
    @Setter
    private ComponentFrame componentFrame;

    /* ■■■■■■■■■■■■■■■■■■■■ Constructors of the component ■■■■■■■■■■■■■■■■■■■■ */

    public MList(ComponentFrame componentFrame, ListModel<E> dataModel) {
        super(dataModel);
        this.componentFrame = componentFrame;
    }

    public MList(ComponentFrame componentFrame, final E[] listData) {
        super(listData);
        this.componentFrame = componentFrame;
    }

    public MList(ComponentFrame componentFrame, final Vector<? extends E> listData) {
        super(listData);
        this.componentFrame = componentFrame;
    }

    public MList(ComponentFrame componentFrame) {
        super();
        this.componentFrame = componentFrame;
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Private utility methods of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Method provides the font rescaling mechanism - uses the fontSizeModifier property
     */
    private void rescaleFont() {
        final float DEFAULT_FONT_SIZE = 32f;
        final float STANDARD_WIDTH = 1000f;
        final float newSize = DEFAULT_FONT_SIZE * fontSizeModifier * (getWidth() / STANDARD_WIDTH);
        setFont(getFont().deriveFont(newSize));
    }
    /* ■■■■■■■■■■■■■■■■■■■■ Method override segment ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param newParentSize The size of the parent component
     * Adjust the position and size of this MList component accordingly to parent component and also rescale font
     */
    @Override
    public void reorganizeComponent(Dimension newParentSize) {
        setBounds(ComponentFrameHandler.generateBoundaries(componentFrame, newParentSize));
        rescaleFont();
    }
}

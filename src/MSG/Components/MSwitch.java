package MSG.Components;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Imports from within the project
import MSG.Miscellaneous.GenericImage;
import MSG.DataTransformation.ComponentFrame;

// Import Java AWT
import java.awt.*;

// Import Mouse Adapter
import java.awt.event.MouseAdapter;

// Import Buffered Image
import java.awt.image.BufferedImage;

/**
 * @author Michal Bureš 2020-2022
 * Modification of MButton that works as a double state switch
 */
public class MSwitch extends MPanel {

    /* ■■■■■■■■■■■■■■■■■■■■ Component fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * MLabel subcomponent of this MButton - can display text over the MButton
     */
    private MLabel switchLabel = null;

    /**
     * Visual representation of this MSwitch in inactive state (can be updated at runtime using the updateGraphics() method)
     */
    private BufferedImage switchGraphics;

    /**
     * Visual representation of this MSwitch in active state (can be updated at runtime using the updateGraphics() method)
     */
    private BufferedImage switchGraphicsActive;

    /**
     * Is the switch ready to function? Ready to be pressed?
     */
    public boolean isActive;

    /* ■■■■■■■■■■■■■■■■■■■■ Various constructors of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the parent component
     * @param isLabeled Should there be a MLabel subcomponent displaying text over the MSwitch?
     * @param switchGraphics Visual representation of this MSwitch in inactive state
     * @param switchGraphicsActive Visual representation of this MSwitch in active state
     * @param fontPath System path pointing to a font file - the font will be loaded and used by the component
     * Heavy constructor - allows specifying font
     */
    public MSwitch(
            ComponentFrame componentFrame,
            boolean isLabeled,
            boolean isActiveImmediately,
            BufferedImage switchGraphics,
            BufferedImage switchGraphicsActive,
            String fontPath
    ) {
        super(componentFrame, null);
        this.isActive = isActiveImmediately;
        registerReactMethod();
        if (isLabeled) {
            installSwitchLabel(fontPath);
        }
        if ((this.switchGraphics = switchGraphics) == null || (this.switchGraphicsActive = switchGraphicsActive) == null) {
            setupDefaultGraphics();
        }
    }

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the parent component
     * @param isLabeled Should there be a MLabel subcomponent displaying text over the MSwitch?
     * @param switchGraphics Visual representation of this MSwitch in inactive state
     * @param switchGraphicsActive Visual representation of this MSwitch in active state
     * Standard constructor - comprises the basic set of instance properties
     */
    public MSwitch(
            ComponentFrame componentFrame,
            boolean isLabeled,
            boolean isActiveImmediately,
            BufferedImage switchGraphics,
            BufferedImage switchGraphicsActive
    ) {
        super(componentFrame, null);
        this.isActive = isActiveImmediately;
        registerReactMethod();
        if (isLabeled) {
            installSwitchLabel(null);
        }
        if ((this.switchGraphics = switchGraphics) == null || (this.switchGraphicsActive = switchGraphicsActive) == null) {
            setupDefaultGraphics();
        }
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Private utility methods of the component initialization ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Register the overridable 'react()' method using mouse event interface
     */
    private void registerReactMethod() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                react();
            }
        });
    }

    /**
     * @param fontPath System path pointing to a font file - the font will be loaded and used by the subcomponent (Optional)
     * Install the MLabel subcomponent if requested - uses the isLabeled property
     */
    private void installSwitchLabel(String fontPath) {
        switchLabel = fontPath != null ? new MLabel(ComponentFrame.ofParentComponent(), fontPath) : new MLabel(ComponentFrame.ofParentComponent());
        add(switchLabel);
    }

    /**
     * Set up the default visual representation of the MSwitch if necessary
     */
    private void setupDefaultGraphics() {
        switchGraphics = new GenericImage(GenericImage.DEFAULT_FILL);
        switchGraphicsActive = new GenericImage(GenericImage.DEFAULT_HIGHLIGHT);
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Public API methods - MLabel subcomponent access ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param text New value for the text property
     * Set the text property of MLabel subcomponent if installed
     */
    public void setLabelText(String text) {
        if (switchLabel != null) {
            switchLabel.setText(text);
        }
    }

    /**
     * @return Text property value of MLabel subcomponent if installed, otherwise empty String
     * Obtain the text property value of MLabel subcomponent if installed
     */
    public String getLabelText() {
        if (switchLabel != null) {
            return switchLabel.getText();
        } else {
            return "";
        }
    }

    /**
     * @param fontSizeModifier New value for the fontSizeModifier property
     * Set the fontSizeModifier property of MLabel subcomponent if installed
     */
    public void setLabelFontSizeModifier(float fontSizeModifier) {
        if (switchLabel != null) {
            switchLabel.setFontSizeModifier(fontSizeModifier);
        }
    }

    /**
     * @param foregroundColor New value for the foreground property
     * Set the foreground property of MLabel subcomponent if installed
     */
    public void setLabelTextColor(Color foregroundColor) {
        if (switchLabel != null) {
            switchLabel.setForeground(foregroundColor);
        }
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Component property update at runtime ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param switchGraphics New visual representation of the MSwitch in inactive state
     * @param switchGraphicsActive New visual representation of the MSwitch in active state
     * Change the visual representation of the MSwitch at runtime - then repaint()
     */
    public void updateGraphics(BufferedImage switchGraphics, BufferedImage switchGraphicsActive) {
        this.switchGraphics = switchGraphics;
        this.switchGraphicsActive = switchGraphicsActive;
        repaint();
    }

    /* ■■■■■■■■■■■■■■■■■■■■ React method ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * The code that should be executed upon mouse click.
     */
    public void react() {
        if (reaction != null) reaction.run();
    }

    // The default on click reaction of MSwitch is the switch of the state property 'isActive'.
    private Runnable reaction = () -> this.isActive = !this.isActive;

    /**
     * Register a runnable instead of overriding the react() method.
     * @param onClickReaction The runnable to be registered
     */
    public void addOnClickReaction(Runnable onClickReaction) {
        this.reaction = onClickReaction;
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Method override segment ■■■■■■■■■■■■■■■■■■■■ */

    @Override
    public void paint(Graphics g) {
        g.drawImage(isActive ? switchGraphicsActive : switchGraphics, 0, 0, getWidth(), getHeight(), null);
        paintChildren(g);
    }
}

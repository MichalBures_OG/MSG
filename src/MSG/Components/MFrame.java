package MSG.Components;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Imports from within the project
import MSG.DataProcessing.ComponentFrameHandler;
import MSG.DataTransformation.ComponentFrame;
import MSG.Miscellaneous.ReorganizationCapable;
import MSG.Miscellaneous.ResolutionControl;

// Project Lombok imports
import lombok.Getter;
import lombok.Setter;

// Import Java AWT
import java.awt.*;

// Import Buffered Image
import java.awt.image.BufferedImage;

// Imports of various Java utilities
import java.util.stream.Stream;

/**
 * @author Michal Bureš 2020-2022
 * The main underlying component of the application - a system window
 */
public class MFrame extends javax.swing.JFrame implements ReorganizationCapable {

    /* ■■■■■■■■■■■■■■■■■■■■ Component fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Is this MFrame needed for the application to run?
     */
    @Setter
    @Getter
    private boolean isCrucial;

    /**
     * Does this MFrame use the standard system based decoration? (MSG works better with custom decoration)
     */
    private final boolean isDecorated;

    /**
     * Component frame instance used by this MFrame
     */
    @Getter
    @Setter
    private ComponentFrame componentFrame;

    /**
     * Active resolution control utility instance (not necessary - useful only when custom monitor is selected)
     */
    public ResolutionControl assignedResolutionControl = null;

    /* ■■■■■■■■■■■■■■■■■■■■ Various constructors of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the environment
     * @param isCrucial Is this MFrame needed for the application to run?
     * @param isDecorated Does this MFrame use the standard system based decoration?
     * @param frameTitle Title of this MFrame
     * @param frameIcon Icon of this MFrame
     * Heavy constructor - allows specifying MFrame icon and title
     */
    public MFrame(
            ComponentFrame componentFrame,
            boolean isCrucial,
            boolean isDecorated,
            String frameTitle,
            BufferedImage frameIcon
    ) {
        this.componentFrame = componentFrame;
        this.isCrucial = isCrucial;
        this.isDecorated = isDecorated;
        configureProperties(frameTitle, frameIcon);
    }

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the environment
     * @param isCrucial Is this MFrame needed for the application to run?
     * @param isDecorated Does this MFrame use the standard system based decoration?
     * @param frameTitle Title of this MFrame
     * Mid-tier constructor - allows specifying MFrame title
     */
    public MFrame(
            ComponentFrame componentFrame,
            boolean isCrucial,
            boolean isDecorated,
            String frameTitle
    ) {
        this.componentFrame = componentFrame;
        this.isCrucial = isCrucial;
        this.isDecorated = isDecorated;
        configureProperties(frameTitle);
    }

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the environment
     * @param isCrucial Is this MFrame needed for the application to run?
     * @param isDecorated Does this MFrame use the standard system based decoration?
     * Lightweight constructor - comprises only the basic specification
     */
    public MFrame(
            ComponentFrame componentFrame,
            boolean isCrucial,
            boolean isDecorated
    ) {
        this.componentFrame = componentFrame;
        this.isCrucial = isCrucial;
        this.isDecorated = isDecorated;
        configureBasics();
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Private utility methods of the component initialization ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param frameTitle Title of this MFrame
     * @param frameIcon Icon of this MFrame
     * Set the MFrame title and icon, then set up the basics
     */
    private void configureProperties(String frameTitle, BufferedImage frameIcon) {
        configureBasics();
        setTitle(frameTitle);
        setIconImage(frameIcon);
    }

    /**
     * @param frameTitle Title of this MFrame
     * Set the MFrame title, then set up the basics
     */
    private void configureProperties(String frameTitle) {
        configureBasics();
        setTitle(frameTitle);
    }

    /**
     * Set up the basic properties of this new MFrame instance
     */
    private void configureBasics() {
        setResizable(false);
        setDefaultCloseOperation(isCrucial ? EXIT_ON_CLOSE : DISPOSE_ON_CLOSE);
        setUndecorated(!isDecorated);
        getContentPane().setLayout(new javax.swing.GroupLayout(getContentPane()));
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Public API methods ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Safely terminate this MFrame - the isCrucial property will be taken into account
     */
    public void terminateFrame() {
        if (isCrucial) {
            System.exit(0);
        } else {
            dispose();
        }
    }

    /**
     * Shortcut method for minimizing an MFrame instance.
     */
    public void minimize() {
        setExtendedState(MFrame.ICONIFIED);
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Method override segment ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param newParentSize The size of the parent component
     * Adjust the position and size of this MFrame component accordingly to the environment
     * <p>
     * The point of origin of the MFrame component can be shifted to an origin of a specific monitor
     * but one has to specify the active ResolutionControl class instance first
     */
    @Override
    public void reorganizeComponent(Dimension newParentSize) {

        Rectangle standardBoundaries = ComponentFrameHandler.generateBoundaries(componentFrame, newParentSize);

        if (assignedResolutionControl != null) {
            GraphicsDevice specificMonitor = assignedResolutionControl.getSpecificMonitor();
            if (specificMonitor != null) {
                Point specificMonitorOrigin = specificMonitor.getDefaultConfiguration().getBounds().getLocation();
                setBounds(new Rectangle(
                        standardBoundaries.x + specificMonitorOrigin.x,
                        standardBoundaries.y + specificMonitorOrigin.y,
                        standardBoundaries.width,
                        standardBoundaries.height
                ));
            } else {
                setBounds(standardBoundaries);
            }
        } else {
            setBounds(standardBoundaries);
        }
    }

    /**
     * Pass the reorganization call to the content pane components that implement the ReorganizationCapable interface
     */
    @Override
    public void reorganizeChildren() {
        Stream.of(getContentPane().getComponents())
                .filter(childComponent -> childComponent instanceof ReorganizationCapable)
                .forEach(childComponent -> ((ReorganizationCapable) childComponent).reorganize(getSize()));
    }

    /**
     * Pass the update call to the content pane components that implement the ReorganizationCapable interface
     */
    @Override
    public void updateComponent() {
        Stream.of(getContentPane().getComponents())
                .filter(component -> component instanceof ReorganizationCapable)
                .forEach(component -> ((ReorganizationCapable) component).updateComponent());
    }

    @Override
    public void paint(Graphics g) {
        paintComponents(g);
    }
}

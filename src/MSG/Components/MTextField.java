package MSG.Components;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Project Lombok imports
import lombok.Getter;
import lombok.Setter;

// Imports from within the project
import MSG.Miscellaneous.ReorganizationCapable;
import MSG.DataTransformation.ComponentFrame;
import MSG.DataProcessing.ComponentFrameHandler;

// Import Java AWT
import java.awt.*;

// Import Java IO
import java.io.*;

// Import Java Swing
import javax.swing.*;

// Imports of various Java utilities
import java.util.Objects;

/**
 * @author Michal Bureš 2020-2022
 * Enhanced variant of Swing JTextField - comes with component scaling and font scaling
 */
public class MTextField extends JTextField implements ReorganizationCapable {

    /* ■■■■■■■■■■■■■■■■■■■■ Component fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * How big should the font be? (This property must be set - otherwise the font will not be visible!)
     */
    @Setter
    private float fontSizeModifier = 0f;

    /**
     * Component frame instance used by this MTextField
     */
    @Getter
    @Setter
    private ComponentFrame componentFrame;

    /* ■■■■■■■■■■■■■■■■■■■■ Various constructors of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the parent component
     * @param fontPath System path pointing to a font file - the font will be loaded and used by the component
     * @param fontSizeModifier A higher number produces bigger font ... values from 1.0 to about 10.0 should be fine
     * Heavy constructor - allows specifying font and font size
     */
    public MTextField(ComponentFrame componentFrame, String fontPath, float fontSizeModifier) {
        super();
        this.componentFrame = componentFrame;
        setFontSizeModifier(fontSizeModifier);
        setProperties(fontPath);
    }

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the parent component
     * @param fontPath System path pointing to a font file - the font will be loaded and used by the component
     * Mid-tier constructor - allows specifying font
     */
    public MTextField(ComponentFrame componentFrame, String fontPath) {
        super();
        this.componentFrame = componentFrame;
        setProperties(fontPath);
    }

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the parent component
     * Lightweight constructor - comprises only the basic specification
     */
    public MTextField(ComponentFrame componentFrame) {
        super();
        this.componentFrame = componentFrame;
        setProperties("");
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Private utility methods of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param fontPath System path pointing to a font file - the font will be loaded and used by the component
     * Method comprises the attempt to load up the font file and the setup of other component properties
     */
    private void setProperties(String fontPath) {
        try {
            setFont(Font.createFont(Font.TRUETYPE_FONT, Objects.requireNonNull(getClass().getResourceAsStream(fontPath))));
        } catch (FontFormatException | IOException | NullPointerException ignored) {
        }
        setHorizontalAlignment(MTextField.CENTER);
    }

    /**
     * Method provides the font rescaling mechanism - uses the fontSizeModifier property
     */
    private void rescaleFont() {
        final float DEFAULT_FONT_SIZE = 32f;
        final float STANDARD_WIDTH = 1000f;
        final float newSize = DEFAULT_FONT_SIZE * fontSizeModifier * (getWidth() / STANDARD_WIDTH);
        setFont(getFont().deriveFont(newSize));
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Method override segment ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param newParentSize The size of the parent component
     * Adjust the position and size of this MTextField component accordingly to parent component and also rescale font
     */
    @Override
    public void reorganizeComponent(Dimension newParentSize) {
        setBounds(ComponentFrameHandler.generateBoundaries(componentFrame, newParentSize));
        rescaleFont();
    }
}

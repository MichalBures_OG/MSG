package MSG.Components;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Project Lombok imports
import lombok.Getter;
import lombok.Setter;

// Imports from within the project
import MSG.Miscellaneous.GenericImage;
import MSG.Miscellaneous.ReorganizationCapable;
import MSG.DataTransformation.ComponentFrame;
import MSG.DataProcessing.ComponentFrameHandler;

// Import Java AWT
import java.awt.*;

// Import Buffered Image
import java.awt.image.BufferedImage;

// Import Java Swing
import javax.swing.*;

// Imports of various Java utilities
import java.util.stream.Stream;

/**
 * @author Michal Bureš 2020-2022
 * Enhanced and graphically customizable interface panel with a transparency feature
 */
public class MPanel extends JLayeredPane implements ReorganizationCapable {

    /* ■■■■■■■■■■■■■■■■■■■■ Component fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Component frame instance used by this MPanel
     */
    @Getter
    @Setter
    private ComponentFrame componentFrame;

    /**
     * Visual representation of this MPanel (can be updated at runtime using the updateGraphics() method)
     */
    private BufferedImage panelGraphics;

    /* ■■■■■■■■■■■■■■■■■■■■ Constructor of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to parent component
     * @param panelGraphics Visual representation of this MPanel
     * Standard constructor - comprises the basic set of instance properties
     */
    public MPanel(ComponentFrame componentFrame, BufferedImage panelGraphics) {
        this.componentFrame = componentFrame;
        setLayout();
        if ((this.panelGraphics = panelGraphics) == null) {
            setupDefaultGraphics();
        }
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Private utility methods of the component initialization ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Set the Swing layout to 'group layout' - works best for MSG
     */
    private void setLayout() {
        setLayout(new javax.swing.GroupLayout(this));
    }

    /**
     * Set up the default visual representation of the MPanel if necessary
     */
    private void setupDefaultGraphics() {
        panelGraphics = new GenericImage(GenericImage.DEFAULT_FILL);
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Public API methods ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param panelGraphics New visual representation of the MPanel
     * Change the visual representation of the MPanel at runtime - then repaint()
     */
    public void updateGraphics(BufferedImage panelGraphics) {
        this.panelGraphics = panelGraphics;
        repaint();
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Method override segment ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param newParentSize The size of the parent component
     * Adjust the position and size of this MPanel component accordingly to the parent component
     */
    @Override
    public void reorganizeComponent(Dimension newParentSize) {
        setBounds(ComponentFrameHandler.generateBoundaries(componentFrame, newParentSize));
    }

    /**
     * Pass the reorganization call to the MPanel components that implement the ReorganizationCapable interface
     */
    @Override
    public void reorganizeChildren() {
        Stream.of(getComponents())
                .filter(childComponent -> childComponent instanceof ReorganizationCapable)
                .forEach(childComponent -> ((ReorganizationCapable) childComponent).reorganize(getSize()));
    }

    /**
     * Pass the update call to the MPanel components that implement the ReorganizationCapable interface
     */
    @Override
    public void updateComponent() {
        Stream.of(getComponents())
                .filter(component -> component instanceof ReorganizationCapable)
                .forEach(component -> ((ReorganizationCapable) component).updateComponent());
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(panelGraphics, 0, 0, getWidth(), getHeight(), null);
        paintChildren(g);
    }
}

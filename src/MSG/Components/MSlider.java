package MSG.Components;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Imports from within the project
import MSG.Miscellaneous.GenericImage;
import MSG.DataTransformation.ComponentFrame;

// Import Java AWT
import java.awt.*;

// Import Mouse Adapter
import java.awt.event.MouseAdapter;

// Import Mouse Event
import java.awt.event.MouseEvent;

// Import Buffered Image
import java.awt.image.BufferedImage;

/**
 * @author Michal Bureš 2020-2022
 * Enhanced and visually customizable alternative to Swing JSlider
 */
public class MSlider extends MPanel {

    /* ■■■■■■■■■■■■■■■■■■■■ Component fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Heavily customized, draggable MButton subcomponent
     */
    private MButton sliderObject;

    /**
     * Primary property of the slider component - the value the slider holds and represents
     */
    private double sliderValue = 0.5d;

    /**
     * Internal integer buffer for the mouse drag processing
     */
    private int MousePosition = -1;

    /* ■■■■■■■■■■■■■■■■■■■■ Constructor of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the parent component
     * @param sliderGraphics Visual representation of the base component
     * @param sliderObjectGraphics Visual representation of the slider object subcomponent in normal state
     * @param sliderObjectGraphicsHover Visual representation of the slider object when dragged by mouse
     * Standard constructor - comprises the basic set of instance properties
     */
    public MSlider(ComponentFrame componentFrame,
                   BufferedImage sliderGraphics,
                   BufferedImage sliderObjectGraphics,
                   BufferedImage sliderObjectGraphicsHover
    ) {
        super(componentFrame, sliderGraphics);
        addSliderObject(sliderObjectGraphics, sliderObjectGraphicsHover);

        if (sliderGraphics == null || sliderObjectGraphics == null || sliderObjectGraphicsHover == null) {
            setupDefaultGraphics();
        }
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Private utility methods of the component initialization ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param sliderObjectGraphics Visual representation of the slider object subcomponent in normal state (passed in by constructor)
     * @param sliderObjectGraphicsHover Visual representation of the slider object when dragged by mouse (passed in by constructor)
     * Setup and add the slider object subcomponent
     */
    private void addSliderObject(BufferedImage sliderObjectGraphics, BufferedImage sliderObjectGraphicsHover) {

        sliderObject = new MButton(null, true, false, sliderObjectGraphics, sliderObjectGraphicsHover) {

            @Override
            public void reorganizeComponent(Dimension newParentSize) {
                setBounds(getX(), getY(), (int) Math.round(newParentSize.width * 0.1d), newParentSize.height);
                relocateSliderObject(false, null);
            }
        };

        sliderObject.addMouseMotionListener(new MouseAdapter() {

            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                relocateSliderObject(true, evt);
            }
        });

        add(sliderObject);
    }

    /**
     * Set up the default visual representation of the MSlider if necessary
     */
    private void setupDefaultGraphics() {
        super.updateGraphics(new GenericImage(GenericImage.DEFAULT_FILL));
        sliderObject.updateGraphics(new GenericImage(GenericImage.SECONDARY_FILL), new GenericImage(GenericImage.DEFAULT_HIGHLIGHT));
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Inner workings of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param userInvoked Was this subcomponent relocation invoked by user (by mouse drag)?
     * @param evt The mouse event information object
     * Versatile utility method - relocates the slider object subcomponent within the MSlider
     */
    private void relocateSliderObject(boolean userInvoked, MouseEvent evt) {
        int currentSize = (int) Math.round(getWidth() * 0.9d);

        if (userInvoked) {
            int previousPosition = MousePosition;
            MousePosition = evt.getXOnScreen();
            int vector = previousPosition != -1 ? MousePosition - previousPosition : 0;

            double sliderValueChange = (double) vector / currentSize;
            double correctionModifier;

            if (sliderValue + sliderValueChange < 0 || sliderValue + sliderValueChange > 1) {
                if (sliderValue + sliderValueChange < 0) {
                    correctionModifier = (sliderValue + sliderValueChange) * -1d;
                } else {
                    correctionModifier = (sliderValue + sliderValueChange - 1d) * -1d;
                }
            } else {
                correctionModifier = 0d;
            }

            sliderValue += (sliderValueChange + correctionModifier);
            MousePosition += Math.round(correctionModifier * (float) currentSize);
        } else {
            MousePosition = -1;
        }

        sliderObject.setBounds((int) Math.round(currentSize * sliderValue), 0, getWidth(), getHeight());
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Public API ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @return Value of the slider (of the primary property) as a double data type - return value from 0.0 to 1.0
     * Getter method for the slider value property
     */
    public double getSliderValue() {
        double returnValue = sliderValue;

        returnValue = Math.max(returnValue, 0d);
        returnValue = Math.min(returnValue, 1d);

        return returnValue;
    }

    /**
     * @param newSliderValue A new slider value as a double data type - value must be from 0.0 to 1.0
     * Setter method for the slider value property
     */
    public void setSliderValue(double newSliderValue) {

        newSliderValue = Math.max(newSliderValue, 0d);
        newSliderValue = Math.min(newSliderValue, 1d);

        sliderValue = newSliderValue;
        relocateSliderObject(false, null);
    }

    /**
     * @param sliderGraphics Visual representation of the base component
     * @param sliderObjectGraphics Visual representation of the slider object subcomponent in normal state
     * @param sliderObjectGraphicsHover Visual representation of the slider object when dragged by mouse
     * Change the visual representation of the MSlider at runtime - then repaint()
     */
    public void updateGraphics(BufferedImage sliderGraphics, BufferedImage sliderObjectGraphics, BufferedImage sliderObjectGraphicsHover) {
        super.updateGraphics(sliderGraphics);
        sliderObject.updateGraphics(sliderObjectGraphics, sliderObjectGraphicsHover);
    }
}

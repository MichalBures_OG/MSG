package MSG.Components;

/* ■■■■■■■■■■■■■■■■■■■■ Imports ■■■■■■■■■■■■■■■■■■■■ */

// Imports from within the project
import MSG.Miscellaneous.GenericImage;
import MSG.DataTransformation.ComponentFrame;

// Import Java AWT
import java.awt.*;

// Import Mouse Adapter
import java.awt.event.MouseAdapter;

// Import Buffered Image
import java.awt.image.BufferedImage;

/**
 * @author Michal Bureš 2020-2022
 * Enhanced and visually customizable alternative to Swing JButton
 */
public class MButton extends MPanel {

    /* ■■■■■■■■■■■■■■■■■■■■ Component fields ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Should this component react to mouse movement over it?
     */
    private final boolean isInteractive;

    /**
     * MLabel subcomponent of this MButton - can display text over the MButton
     */
    private MLabel buttonLabel = null;

    /**
     * Visual representation of this MButton in normal state (can be updated at runtime using the updateGraphics() method)
     */
    private BufferedImage buttonGraphics;

    /**
     * Visual representation of this MButton when mouse cursor travels over it (can be updated at runtime using the updateGraphics() method)
     */
    private BufferedImage buttonGraphicsHover;

    /**
     * Is mouse cursor positioned over this component?
     */
    public boolean onHover = false;

    /* ■■■■■■■■■■■■■■■■■■■■ Various constructors of the component ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the parent component
     * @param isInteractive Should this component react to mouse movement over it?
     * @param isLabeled Should there be a MLabel subcomponent displaying text over the MButton?
     * @param buttonGraphics Visual representation of this MButton in normal state
     * @param buttonGraphicsHover Visual representation of this MButton when mouse cursor travels over it
     * @param fontPath System path pointing to a font file - the font will be loaded and used by the component
     * Heavy constructor - allows specifying font
     */
    public MButton(
            ComponentFrame componentFrame,
            boolean isInteractive,
            boolean isLabeled,
            BufferedImage buttonGraphics,
            BufferedImage buttonGraphicsHover,
            String fontPath
    ) {
        super(componentFrame, null);
        this.isInteractive = isInteractive;
        registerMouseEvents();
        if (isLabeled) {
            installButtonLabel(fontPath);
        }
        if ((this.buttonGraphics = buttonGraphics) == null || (this.buttonGraphicsHover = buttonGraphicsHover) == null) {
            setupDefaultGraphics();
        }
    }

    /**
     * @param componentFrame Component frame instance - point of origin and size of the component relative to the parent component
     * @param isInteractive Should this component react to mouse movement over it?
     * @param isLabeled Should there be a MLabel subcomponent displaying text over the MButton?
     * @param buttonGraphics Visual representation of this MButton in normal state
     * @param buttonGraphicsHover Visual representation of this MButton when mouse cursor travels over it
     * Standard constructor - comprises the basic set of instance properties
     */
    public MButton(
            ComponentFrame componentFrame,
            boolean isInteractive,
            boolean isLabeled,
            BufferedImage buttonGraphics,
            BufferedImage buttonGraphicsHover
    ) {
        super(componentFrame, null);
        this.isInteractive = isInteractive;
        registerMouseEvents();
        if (isLabeled) {
            installButtonLabel(null);
        }
        if ((this.buttonGraphics = buttonGraphics) == null || (this.buttonGraphicsHover = buttonGraphicsHover) == null) {
            setupDefaultGraphics();
        }
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Private utility methods of the component initialization ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * Register the mouse event interface - implement mouse event reaction methods
     */
    private void registerMouseEvents() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                if (isInteractive) {
                    toggleOnHoverState();
                }
            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {
                if (isInteractive) {
                    toggleOnHoverState();
                }
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                react();
            }
        });
    }

    /**
     * Toggle the onHover boolean property state and change visuals accordingly right after
     */
    private void toggleOnHoverState() {
        onHover = !onHover;
        repaint();
    }

    /**
     * @param fontPath System path pointing to a font file - the font will be loaded and used by the subcomponent (Optional)
     * Install the MLabel subcomponent if requested - uses the isLabeled property
     */
    private void installButtonLabel(String fontPath) {
        buttonLabel = fontPath != null ? new MLabel(ComponentFrame.ofParentComponent(), fontPath) : new MLabel(ComponentFrame.ofParentComponent());
        add(buttonLabel);
    }

    /**
     * Setup the default visual representation of the MButton if necessary
     */
    private void setupDefaultGraphics() {
        buttonGraphics = new GenericImage(GenericImage.DEFAULT_FILL);
        buttonGraphicsHover = new GenericImage(GenericImage.DEFAULT_HIGHLIGHT);
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Public API methods - MLabel subcomponent access ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param text New value for the text property
     * Set the text property of MLabel subcomponent if installed
     */
    public void setLabelText(String text) {
        if (buttonLabel != null) {
            buttonLabel.setText(text);
        }
    }

    /**
     * @return Text property value of MLabel subcomponent if installed, otherwise empty String
     * Obtain the text property value of MLabel subcomponent if installed
     */
    public String getLabelText() {
        if (buttonLabel != null) {
            return buttonLabel.getText();
        } else {
            return "";
        }
    }

    /**
     * @param fontSizeModifier New value for the fontSizeModifier property
     * Set the fontSizeModifier property of MLabel subcomponent if installed
     */
    public void setLabelFontSizeModifier(float fontSizeModifier) {
        if (buttonLabel != null) {
            buttonLabel.setFontSizeModifier(fontSizeModifier);
        }
    }

    /**
     * @param foregroundColor New value for the foreground property
     * Set the foreground property of MLabel subcomponent if installed
     */
    public void setLabelTextColor(Color foregroundColor) {
        if (buttonLabel != null) {
            buttonLabel.setForeground(foregroundColor);
        }
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Component property update at runtime ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * @param buttonGraphics New visual representation of the MButton in normal state
     * @param buttonGraphicsHover New visual representation of the MButton when mouse cursor travels over it
     * Change the visual representation of the MButton at runtime - then repaint()
     */
    public void updateGraphics(BufferedImage buttonGraphics, BufferedImage buttonGraphicsHover) {
        this.buttonGraphics = buttonGraphics;
        this.buttonGraphicsHover = buttonGraphicsHover;
        repaint();
    }

    /* ■■■■■■■■■■■■■■■■■■■■ React method ■■■■■■■■■■■■■■■■■■■■ */

    /**
     * The code that should be executed upon mouse click.
     */
    public void react() {
        if (reaction != null) reaction.run();
    }

    private Runnable reaction = null;

    /**
     * Register a runnable instead of overriding the react() method.
     * @param onClickReaction The runnable to be registered
     */
    public void addOnClickReaction(Runnable onClickReaction) {
        this.reaction = onClickReaction;
    }

    /* ■■■■■■■■■■■■■■■■■■■■ Method override segment ■■■■■■■■■■■■■■■■■■■■ */

    @Override
    public void paint(Graphics g) {
        g.drawImage(onHover ? buttonGraphicsHover : buttonGraphics, 0, 0, getWidth(), getHeight(), null);
        paintChildren(g);
    }
}
